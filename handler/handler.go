package handler

import (
	"errors"
	"image"
	"image/png"
	"net/http"
	"strconv"

	"gitlab.com/sbelyakov/mandelbrot/worker"
)

const (
	sizeSmall  = "small"
	sizeMedium = "medium"
	sizeBig    = "big"
)

type sizePng struct {
	width  int
	height int
}

var sizeMap = map[string]sizePng{
	sizeSmall:  {width: 64, height: 64},
	sizeMedium: {width: 512, height: 512},
	sizeBig:    {width: 2048, height: 2048},
}

// MandelHandler Обработчик клиентских
type MandelHandler struct {
	x        float64
	y        float64
	zoom     int
	width    int
	height   int
	imgCh    chan *image.RGBA      //Ссылка на канал, в который worker запишет png
	workerCh chan *worker.WorkData //Ссылка на канал, с которым будет работать worker
}

// ServeHTTP
func (fh *MandelHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	//Инициализируем данные для worker'а
	ws := &worker.WorkData{
		PointX: fh.x,
		PointY: fh.y,
		Zoom:   fh.zoom,
		Width:  fh.width,
		Height: fh.height,
		ImgCh:  fh.imgCh,
	}

	//Отправляем данные в worker
	fh.workerCh <- ws

	//Настраиываем декодер на максимальную производительность при сжатии
	enc := png.Encoder{
		CompressionLevel: png.BestSpeed,
		BufferPool:       nil,
	}

	//Т.к. канал синхронный, ждем ответа от воркера
	//TODO можно добавить таймаут на ожидание
	enc.Encode(w, <-fh.imgCh)
	close(fh.imgCh)
}

// Handle Первичная обработка запроса с валидацией и инициализацией основной обработки
func Handle(chBig chan *worker.WorkData, chMediumSmall chan *worker.WorkData) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var x, y float64
		var zoom int
		var size *sizePng
		var err error

		//Блок простейшей валидации входных параметров
		if x, err = strconv.ParseFloat(r.FormValue("x"), 32); err != nil {
			http.Error(w, "wrong param: x", http.StatusBadRequest)
			return
		}
		if y, err = strconv.ParseFloat(r.FormValue("y"), 32); err != nil {
			http.Error(w, "wrong param: y", http.StatusBadRequest)
			return
		}
		if zoom, err = strconv.Atoi(r.FormValue("zoom")); err != nil || zoom <= 0 {
			http.Error(w, "wrong param: zoom", http.StatusBadRequest)
			return
		}
		if size, err = getSizeFromRequest(r); err != nil {
			http.Error(w, "wrong param: res", http.StatusBadRequest)
			return
		}

		//Инициализация основной обработки запроса
		var imgCh = make(chan *image.RGBA) //Канал, в который буде записан результирующий png
		mainHandler := &MandelHandler{
			x:        x,
			y:        y,
			zoom:     zoom,
			width:    size.width,
			height:   size.height,
			workerCh: chMediumSmall,
			imgCh:    imgCh,
		}
		if *size == sizeMap[sizeBig] {
			mainHandler.workerCh = chBig //Для "тяжелых" запросов переопределяем канал
		}

		//run
		mainHandler.ServeHTTP(w, r)
	})
}

func getSizeFromRequest(r *http.Request) (*sizePng, error) {
	sr := r.FormValue("res")
	for k, v := range sizeMap {
		if k == sr {
			return &v, nil
		}
	}
	return nil, errors.New("wrong param: res")
}
