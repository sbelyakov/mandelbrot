package worker

import (
	"bufio"
	"bytes"
	"image"
	"image/png"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMandelbrot(t *testing.T) {
	curDir, err := os.Getwd()
	assert.NoError(t, err)
	img, err := os.Open(curDir + "/testdata/medium.png")
	assert.NoError(t, err)
	defer img.Close()
	imgData, err := ioutil.ReadAll(img)
	assert.NoError(t, err)

	var ch = make(chan *image.RGBA)
	go mandelbrot(0.0, 0.0, 1, 512, 512, ch)

	imgRes := <-ch

	enc := png.Encoder{
		CompressionLevel: png.BestSpeed,
		BufferPool:       nil,
	}

	var b bytes.Buffer
	writer := bufio.NewWriter(&b)
	enc.Encode(writer, imgRes)
	err = writer.Flush()
	assert.NoError(t, err)
	assert.Equal(t, imgData, b.Bytes())
}
