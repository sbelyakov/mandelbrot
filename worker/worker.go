package worker

import (
	"image"
	"image/color"
	"log"
	"math"
)

//WorkData Набор данных для работы worker'а
type WorkData struct {
	ImgCh  chan *image.RGBA //Ссылка на канал, в который worker запишет png
	PointX float64
	PointY float64
	Zoom   int
	Width  int
	Height int
}

// Worker Процесс, высчитывающий mandelbrot png
func Worker(c chan *WorkData) {
	for {
		select {
		case wd, ok := <-c:
			if !ok {
				log.Print("Channel is closed! \n")
				return
			}

			mandelbrot(wd.PointX, wd.PointY, wd.Zoom, wd.Width, wd.Height, wd.ImgCh)
		}
	}
}

//maxMandelValue Оптимальное значение условия вхождения в ряд
var maxMandelValue = 4

// iterations Стартовое количество итераций
var iterations = 30

// mandelbrot расчет png, результат записывает в imgCh
func mandelbrot(pointX float64, pointY float64, zoom int, width int, height int, imgCh chan *image.RGBA) {
	var z, t, c complex128                                //Комплексные числа для расчета
	var x, y int                                          //Координаты пикселя в png
	var n, maxIterations int                              //Счетчики итерациий
	var mx, my int                                        //Центр обсчета
	zoomForComplex := 1 / (float64(zoom) * 100)           //Зум для расчета значений ряда, подобран эмпирическим путем
	img := image.NewRGBA(image.Rect(0, 0, width, height)) //Заготовка png (В качестве background'а альфа-канал)
	mx = width / 2
	my = height / 2
	pointX *= 100 * 2 * float64(zoom) //Переданные координаты Увеличиваются пропорционально зумму
	pointY *= 100 * 2 * float64(zoom)
	maxDiff := 0.01 // Эмперический коэф, который ограничивает кол-во итараций

	//Далее расчет самого изображения
	//Чем быстрее "точка" уходит в бесконечность, тем темнее будет ее цвет
	//Подробнее https://goo.gl/Dq6UuN
	for y = -my; y < my; y++ {
		for x = -mx; x < mx; x++ {
			c = complex((float64(x)+pointX)*zoomForComplex, (float64(y)+pointY)*zoomForComplex)
			z = complex(0, 0)

			maxIterations = iterations
			for n = 0; mandelPif(z) < float64(maxMandelValue) && n < maxIterations; n++ {

				//При больших зуммах, а также на участках с "большой плотностью" необходимо обеспечить большую точность.
				//Используем статистический метод оптимизации с аддаптацией (смотрим скорость на последней итерации)
				if n == iterations-1 {
					if diff := mandelPif(z) - mandelPif(t); diff < 1 && diff > maxDiff {
						maxIterations = int(float64(maxIterations) / diff)
					}
				}
				t = z
				zr := math.Pow(real(t), 2) - math.Pow(imag(t), 2) + real(c)
				zi := 2*real(t)*imag(t) + imag(c)
				z = complex(zr, zi)
			}

			if n < maxIterations {
				clr := uint8(math.Mod(float64(n)*6, 255))
				col := color.RGBA{R: clr, G: clr, B: clr, A: 255}
				img.SetRGBA(mx+x, my+y, col)
			}
		}
	}
	imgCh <- img
}

// mandelPif Вернет сумму квадратов реальной и мнимой части
func mandelPif(z complex128) float64 {
	return math.Pow(real(z), 2) + math.Pow(imag(z), 2)
}
