# Mandelbrot

### Для запуска (варианты)
 - Запустить бинарь, который доступен в артефакте последнего удачного запуска CI
 - Cклонировать проект в $GOPATH/gitlab.com/sbelyakov/ && go run main.go

Получение png с размером 64x64 в точке (-0,7 0) в 10кратном увеличении
```
http://localhost:8080/mandelbrot?x=-0.7&y=0&zoom=10&res=small
```

Получение png с размером 5124x512 в точке (-0,7 0) в 10кратном увеличении
```
http://localhost:8080/mandelbrot?x=-0.7&y=0&zoom=10&res=medium
```

Получение png с размером 2048x2048 в точке (-0,7 0) в 10кратном увеличении

```
http://localhost:8080/mandelbrot?x=-0.7&y=0&zoom=10&res=big
```