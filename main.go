package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/sbelyakov/mandelbrot/handler"
	"gitlab.com/sbelyakov/mandelbrot/worker"
	"gopkg.in/djherbis/fscache.v0"
	"time"
)

func main() {

	//канал для обработки big изображений
	var chBig = make(chan *worker.WorkData)
	//канал для обработки medium.png и small изображений
	var chMediumSmall = make(chan *worker.WorkData)
	defer close(chBig)
	defer close(chMediumSmall)

	go worker.Worker(chBig)
	go worker.Worker(chMediumSmall)

	//Настройка кеширования
	//Кеш позволит мгновенно обрабатывть одинаковые запросы
	//expiry позволит ограничить использование диска
	//Для более точного соблюдения размера диска можно написать  watchdog, который будет следить за местом на диске и
	// в нужный момент делать инвалидацию
	c, err := fscache.New("./cache", 0700, 120 * time.Second)
	if err != nil {
		log.Fatal(err.Error())
	}

	http.Handle("/mandelbrot", fscache.Handler(c, handler.Handle(chBig, chMediumSmall)))

	log.Println("Starting server...")

	httpAddr := ":8080" //TODO можно параметризовать
	log.Printf("HTTP service listening on %s \n ", httpAddr)
	if err := http.ListenAndServe(httpAddr, nil); err != nil {
		log.Fatal(fmt.Errorf("Can not start server: %s \n ", err.Error()))
	}
}
